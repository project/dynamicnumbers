
IfByPhone Dynamic Phone Number Drupal Module

(c)2009 - Licensed under the GPLv2 - See LICENSE.txt for details.

To install...

1. Copy the dynamicnumber module files to your sites/all/modules directory.
Enable the module, in your system menu at Administer > Site building > Modules.

2. Sign up for an account at the IfByPhone web site...
      http://ifbyphone.com

3. Visit your dynamicnumber settings page, under the system menu at Administer >
Site configuration > Dynamic Phone Number. You'll need to enter your IfByPhone API
public key. You can obtain this by logging into your IfByPhone account.

4. (optional) Visit your blocks configuration page, under the system menu at
Administer > Site building > Blocks. Assign the IfByPhone Dynamic Number block
to a region in your page template.

5. (optional) Should you wish to insert a Dynamic Phone Number in content other
than a block, enable the PHP Filter module and use this PHP code...

<?php
/* This shows the number and the pre- and post-number text. */
print dynamicnumber_display();
?>

<?php
/* This shows the bare number with no theme wrapping. */
print dynamicnumber_display(false);
?>

